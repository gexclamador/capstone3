import React, { useState, useEffect } from 'react';
import { Container } from 'react-bootstrap';
import { BrowserRouter as Router } from 'react-router-dom';
import { Route, Routes } from 'react-router-dom';
import { UserProvider } from './UserContext';
import AppNavbar from './components/AppNavbar';
import Error from './pages/Error';
import Home from './pages/Home';
import Register from './pages/Register';
import Login from './pages/Login';
import Logout from './pages/Logout';
import Products from './pages/Products';
import AddProduct from './pages/AddProduct.js';
import ProductView from './pages/ProductView.js';
import Order from './pages/Order.js';


function App() {
  const [user, setUser] = useState({
    id: null,
    isAdmin: false
  });

  const unsetUser = () => {
    localStorage.clear();
  }

useEffect(() => {
    const token = localStorage.getItem('token');
    if (token) {
      const decodedToken = atob(token.split('.')[1]);
      const userFromToken = JSON.parse(decodedToken);
      
      setUser({
        id: userFromToken.id,
        isAdmin: userFromToken.isAdmin
      });
    }
  }, []);


  return (
    <UserProvider value={{ user, setUser, unsetUser }}>
      <Router>
        <AppNavbar />
        <Container>
          <Routes>
            <Route path='/' element={<Home />} />
            <Route path='/register' element={<Register />} />
            <Route path='/products/add' element={<AddProduct/>}/>
            <Route path="/products/:productId" element={<ProductView />} />
            <Route path="/login" element={<Login />} />
            <Route path='/logout' element={<Logout />} />
            <Route path="/products" element={<Products />} />
            <Route path="/orderdetails" element={<Order />} />
            <Route path='*' element={<Error />} />
          </Routes>
        </Container>
      </Router>
    </UserProvider>
  );
}

export default App;


