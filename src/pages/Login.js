import { Form, Button } from 'react-bootstrap';
import { useState, useContext,useEffect } from 'react';
import UserContext from '../UserContext.js';
import { Navigate } from 'react-router-dom';
import Swal from 'sweetalert2';

export default function Login() {
  const { user, setUser } = useContext(UserContext);
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [isActive, setIsActive] = useState(true);

  function authenticate(e) {
    e.preventDefault();

    fetch(`${process.env.REACT_APP_API_URL}/api/users/login`, {
      method: 'POST',
      headers: {
        "Content-Type": "application/json"
      },
      body: JSON.stringify({
        email: email,
        password: password
      })
    })
      .then(response => response.json())
      .then(result => {
        if (result.accessToken) {
          localStorage.setItem('token', result.accessToken);

          // Decode the token to get user information
          const decodedToken = atob(result.accessToken.split('.')[1]);
          const userFromToken = JSON.parse(decodedToken);

          // Set the user state based on the token data
          setUser({
            id: userFromToken.id,
            isAdmin: userFromToken.isAdmin
          });

          // Clear input fields after submission
          setEmail('');
          setPassword('');

          Swal.fire({
            title: 'Login Success',
            text: 'You have logged in successfully!',
            icon: 'success'
          });

        } else {
          Swal.fire({
            title: 'Something went wrong',
            text: `${email} does not exist`,
            icon: 'warning'
          });
        }
      })
  }

  useEffect(() => {
    // Validation to enable submit button when all fields are populated and both passwords match
    if (email !== '' && password !== '') {
      setIsActive(true);
    } else {
      setIsActive(false);
    }
  }, [email, password]);

  return (
    (user.id !== null) ?
      <Navigate to='/products' />
      :
      <Form onSubmit={(e) => authenticate(e)}>
        <h1 className="my-5 text-center">Login</h1>
        <Form.Group controlId="userEmail">
          <Form.Label>Email address</Form.Label>
          <Form.Control
            type="email"
            placeholder="Enter email"
            value={email}
            onChange={(e) => setEmail(e.target.value)}
            required
          />
        </Form.Group>

        <Form.Group controlId="password">
          <Form.Label>Password</Form.Label>
          <Form.Control
            type="password"
            placeholder="Password"
            value={password}
            onChange={(e) => setPassword(e.target.value)}
            required
          />
        </Form.Group>

        <Button variant="primary" type="submit" id="submitBtn" disabled={isActive === false}>
          Submit
        </Button>
      </Form>
  )
}
