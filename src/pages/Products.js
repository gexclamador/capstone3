import React, { useState, useEffect, useContext } from 'react';
import UserContext from '../UserContext';
import AdminView from '../components/AdminView'; 
import UserView from '../components/UserView'; 


export default function Products() { 
  const { user } = useContext(UserContext);
  const [products, setProducts] = useState([]);

  const fetchData = () => {
    fetch(`${process.env.REACT_APP_API_URL}/api/products/all`) 
      .then(res => res.json())
      .then(data => {
        setProducts(data);
      })
  };

  useEffect(() => {
    console.log(user.isAdmin);

    fetchData();
  }, []);

  return (
    <>
      {user.isAdmin ? (
        <AdminView productsData={products} fetchData={fetchData} /> 
      ) : (
        <UserView productsData={products} />
      )}
   </>
  );
}
