import React, { useState, useEffect, useContext } from 'react';
import { Container, Card, Button, Row, Col, Form } from 'react-bootstrap';
import { useParams, useNavigate, Link } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';

export default function ProductView() {
  const { user } = useContext(UserContext);
  const { productId } = useParams();
  const navigate = useNavigate();

  // States
  const [product, setProduct] = useState({
    name: '',
    description: '',
    price: 0,
  });

  const [quantity, setQuantity] = useState(1); // Default quantity is 1

  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/api/products/${productId}`)
      .then((res) => res.json())
      .then((data) => {
        setProduct(data);
      });
  }, [productId]);

  const createOrder = () => {
    fetch(`${process.env.REACT_APP_API_URL}/api/orders/checkout`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${localStorage.getItem('token')}`,
      },
      body: JSON.stringify({
        userId: user.id,
        productId: productId,
        quantity: quantity, // Use the quantity state here
      }),
    })
      .then((res) => res.json())
      .then((data) => {
        if (data.message === 'Order created successfully') {
          Swal.fire({
            title: 'Order Created',
            icon: 'success',
            text: 'Your order has been created successfully.',
          });

          // Redirect the user or perform any other actions after successful order creation
        } else {
          Swal.fire({
            title: 'Error',
            icon: 'error',
            text: 'Something went wrong. Please try again.',
          });
        }
      });
  };

  return (
    <Container className="mt-5">
      <Row>
        <Col lg={{ span: 6, offset: 3 }}>
          <Card>
            <Card.Body className="text-center">
              <Card.Title>{product.name}</Card.Title>

              <Card.Subtitle>Description:</Card.Subtitle>
              <Card.Text>{product.description}</Card.Text>

              <Card.Subtitle>Price:</Card.Subtitle>
              <Card.Text>PHP {product.price}</Card.Text>

              <Form.Group>
                <Form.Label>Quantity:</Form.Label>
                <Form.Control
                  type="number"
                  min="1"
                  value={quantity}
                  onChange={(e) => setQuantity(e.target.value,10)}
                />
              </Form.Group>

              {user.id !== null ? (
                <Button variant="primary" onClick={createOrder}>
                  Order Now!
                </Button>
              ) : (
                <Link className="btn btn-danger" to="/login">
                  Log in to create Order
                </Link>
              )}
            </Card.Body>
          </Card>
        </Col>
      </Row>
    </Container>
  );
}
