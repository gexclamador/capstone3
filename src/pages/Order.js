
import React, { useContext } from 'react';
import UserContext from '../UserContext';
import OrderUserView from '../components/OrderUserView';
import OrderAdminView from '../components/OrderAdminView';

function Order() {
  const { user } = useContext(UserContext);

  return (
    <div>
      {user.isAdmin ? <OrderAdminView /> : <OrderUserView />}
    </div>
  );
}

export default Order;
