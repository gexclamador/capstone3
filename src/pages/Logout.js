import {Navigate} from 'react-router-dom';
import {useContext,useEffect} from 'react';
import UserContext from '../UserContext';

export default function Logout(){
	// localStorage.clear();

  const {unsetUser,setUser} = useContext(UserContext);
    unsetUser();
    
     useEffect(() =>{

     	setUser({
     		id: null,setUserisAdmin:null
     	});

     },[setUser])


	return(
		<Navigate to='/login'/>
		)
}