import React from 'react';
import { Row, Col, Card } from 'react-bootstrap';

export default function Highlights() {
  return (
    <Row className="my-3">
      <Col xs={12} md={4}>
        <Card className="cardHighlight p-3">
          <Card.Body>
            <Card.Title>
              <h2>Discover Captivating Love Stories</h2>
            </Card.Title>
            <Card.Text>
              Immerse yourself in the world of romance with our vast collection of love stories. From heartwarming tales of second chances to passionate whirlwind romances, we have it all.
            </Card.Text>
          </Card.Body>
        </Card>
      </Col>

      <Col xs={12} md={4}>
        <Card className="cardHighlight p-3">
          <Card.Body>
            <Card.Title>
              <h2>Escape into Romantic Worlds</h2>
            </Card.Title>
            <Card.Text>
              Experience the magic of love in different settings and eras. Our romance novels will transport you to enchanting destinations and introduce you to unforgettable characters.
            </Card.Text>
          </Card.Body>
        </Card>
      </Col>

      <Col xs={12} md={4}>
        <Card className="cardHighlight p-3">
          <Card.Body>
            <Card.Title>
              <h2>Find Your Next Book Crush</h2>
            </Card.Title>
            <Card.Text>
              Fall in love with stories that will tug at your heartstrings. Whether you're a fan of sweet romances, steamy encounters, or epic love sagas, there's a book waiting for you.
            </Card.Text>
          </Card.Body>
        </Card>
      </Col>
    </Row>
  );
}
