


import React, { useState, useEffect } from 'react';
import { Row } from 'react-bootstrap';
import ProductCard from './ProductCard';
import SearchProduct from './SearchProduct';

const UserView = ({ productsData }) => {
  const [activeProducts, setActiveProducts] = useState([]);

  useEffect(() => {
    const activeProductsData = productsData.filter(product => product.isActive);
    const activeProductsElements = activeProductsData.map(product => (
      <ProductCard product={product} key={product._id} />
    ));

    setActiveProducts(activeProductsElements);
  }, [productsData]);

  return (
    <div>
      <h2 className="text-center">Products</h2>
      <Row className="justify-content-center">
       <SearchProduct/>
        {activeProducts}
      </Row>
    </div>
  );
};

export default UserView;
