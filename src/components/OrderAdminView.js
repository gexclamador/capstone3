import React, { useState, useEffect } from 'react';

function OrderAdminView() {
  const [orders, setOrders] = useState([]);
  
  useEffect(() => {
    const token = localStorage.getItem('token'); 
    if (!token) {
      console.error('Token not found in localStorage.');
      return;
    }

    fetch(`${process.env.REACT_APP_API_URL}/api/orders`, {
      method: 'GET',
      headers: {
        'Authorization': `Bearer ${token}`, 
      },
    })
      .then(response => response.json())
      .then(data => setOrders(data))
      .catch(error => console.error('Error fetching orders:', error));
  }, []);

  return (
    <div>
      <h1>Order Admin View</h1>
      <div className="table-responsive">
        <table className="table table-bordered table-striped"> 
          <thead>
            <tr>
              <th>Order ID</th>
              <th>User ID</th>
              <th>Products</th>
              <th>Total Amount</th>
              <th>Purchased On</th>
            </tr>
          </thead>
          <tbody>
            {orders.map(order => (
              <tr key={order._id}>
                <td>{order._id}</td>
                <td>{order.userId}</td>
                <td>
                  {order.products.map(product => (
                    <p key={product._id}>
                      {product.productName} (Quantity: {product.quantity})
                    </p>
                  ))}
                </td>
                <td>{order.totalAmount}</td>
                <td>{new Date(order.purchasedOn).toLocaleString()}</td>
              </tr>
            ))}
          </tbody>
        </table>
      </div>
    </div>
  );
}

export default OrderAdminView;
