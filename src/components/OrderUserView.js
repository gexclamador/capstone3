
import React, { useState, useEffect } from 'react';

function OrderUserView() {
  const [userCart, setUserCart] = useState([]);

  useEffect(() => {
    // Fetch user's cart from the backend
    fetch(`${process.env.REACT_APP_API_URL}/api/users/userOrders`, {
      method: 'GET',
      headers: {
        'Authorization': `Bearer ${localStorage.getItem('token')}`, 
      },
    })
      .then((res) => res.json())
      .then((data) => {
        if (Array.isArray(data.cart)) {
          setUserCart(data.cart);
        } else {
          console.error('Invalid user cart data:', data);
        }
      })
      .catch((error) => {
        console.error('Error fetching user cart:', error);
      });
  }, []);

  return (
    <div>
      <h1>User's Cart</h1>
      <table className="table">
        <thead>
          <tr>
            <th>Product Name</th>
            <th>Description</th>
            <th>Price</th>
            <th>Quantity</th>
            <th className="text-center"> {/* Center-align header content */}
              <div>Total Price</div>
            </th>
          </tr>
        </thead>
        <tbody>
          {userCart.map((cartItem) => (
            <tr key={cartItem.productId && cartItem.productId._id}>
              <td>{cartItem.productId && cartItem.productId.name}</td>
              <td>{cartItem.productId && cartItem.productId.description}</td>
              <td>PHP {cartItem.productId && cartItem.productId.price}</td>
              <td>{cartItem.quantity}</td>
              <td className="text-center">${(cartItem.productId && cartItem.productId.price) * cartItem.quantity}</td> {/* Center-align data content */}
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
}


export default OrderUserView;
























