import React from 'react';
import { Button } from 'react-bootstrap';
import Swal from 'sweetalert2';

export default function ArchiveProduct({ productId, isActive, fetchData }) {
  const archiveToggle = () => {
    fetch(`${process.env.REACT_APP_API_URL}/api/products/${productId}/archive`, {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${localStorage.getItem('token')}`,
      },
    })
      .then((response) => response.json())
      .then((data) => {
        console.log(data);
        if (data === true) {
          Swal.fire({
            title: 'Product Archived!',
            text: 'Product successfully archived.',
            icon: 'success',
          });
          fetchData(); 
        } else {
          Swal.fire({
            title: 'Something went wrong',
            text: 'Please try again.',
            icon: 'error',
          });
          fetchData(); 
        }
      });
  };

  const activateToggle = () => {
    console.log('Archiving product with ID:', productId);
  
    fetch(`${process.env.REACT_APP_API_URL}/api/products/${productId}/activate`, {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${localStorage.getItem('token')}`,
      },
    })
      .then((response) => response.json())
      .then((data) => {
        console.log(data);
        if (data === true) {
          Swal.fire({
            title: 'Product Activated!',
            text: 'Product successfully activated.',
            icon: 'success',
          });
          fetchData();
        } else {
          Swal.fire({
            title: 'Something went wrong',
            text: 'Please try again.',
            icon: 'error',
          });
          fetchData(); 
        }
      });
  };

  return (
    <>
      {isActive ? (
        <Button variant="danger" size="sm" onClick={archiveToggle}>
          Archive
        </Button>
      ) : (
        <Button variant="success" size="sm" onClick={activateToggle}>
          Activate
        </Button>
      )}
    </>
  );
}
