import React from 'react';
import { Row, Col, Button } from 'react-bootstrap';
import { Link } from 'react-router-dom';

export default function ErrorBanner() {
  return (
    <Row>
      <Col className="p-5 text-center">
        <h1>404 Error</h1>
        <p>The page that you are looking for cannot be found.</p>
        <Button variant="primary" as={Link} to="/">Go to Home</Button>
      </Col>
    </Row>
  );
}
