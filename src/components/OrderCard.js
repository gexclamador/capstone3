import React, { useState, useEffect, useContext } from 'react';
import UserContext from '../UserContext';
import OrdersAdminView from '../components/OrdersAdminView';
import OrdersUserView from '../components/OrdersUserView';

export default function Orders() {
  const { user } = useContext(UserContext);
  const [orders, setOrders] = useState([]);

  const fetchOrders = () => {
    fetch(`${process.env.REACT_APP_API_URL}/api/orders/userOrders`, {
      method: 'GET',
      headers: {
        Authorization: `Bearer ${user.token}` 
      }
    })
      .then(res => res.json())
      .then(data => {
        setOrders(data.cart);
      })
      .catch(error => {
        console.error('Error fetching user orders:', error);
      });
  };

  useEffect(() => {
    if (user.isAdmin) {
      fetchOrders();
    }
  }, [user.isAdmin]);

  return (
    <>
      {user.isAdmin ? (
        <OrdersAdminView ordersData={orders} fetchOrders={fetchOrders} />
      ) : (
        <OrdersUserView ordersData={orders} />
      )}
    </>
  );
}
