import React, { useState } from 'react';

const SearchProduct = () => {
  const [minPrice, setMinPrice] = useState('');
  const [maxPrice, setMaxPrice] = useState('');
  const [searchQuery, setSearchQuery] = useState('');
  const [searchResults, setSearchResults] = useState([]);

  const handleSearchByName = async () => {
    try {
      const response = await fetch(`${process.env.REACT_APP_API_URL}/api/products/searchByName`, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({ productName: searchQuery })
      });
      if (response.ok) {
        const data = await response.json();
        setSearchResults(data);
      } else {
        console.error('Error searching for products by name:', response.status);
      }
    } catch (error) {
      console.error('Error searching for products by name:', error);
    }
  };

  const handleSearchByPrice = async () => {
    try {
      const response = await fetch(`${process.env.REACT_APP_API_URL}/api/products/searchByPriceRange`, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({ minPrice, maxPrice })
      });
      if (response.ok) {
        const data = await response.json();
        setSearchResults(data);
      } else {
        console.error('Error searching for products by price:', response.status);
      }
    } catch (error) {
      console.error('Error searching for products by price:', error);
    }
  };

  return (
    <div>
      <h2>ebook Search</h2>
      <div className="form-group">
        <label htmlFor="productName">ebook title:</label>
        <input
          type="text"
          id="productName"
          className="form-control"
          value={searchQuery}
          onChange={event => setSearchQuery(event.target.value)}
        />
      </div>
      <button className="btn btn-primary" onClick={handleSearchByName}>
        Search by Name
      </button>
      <div className="form-group">
        <label htmlFor="minPrice">Min Price:</label>
        <input
          type="number"
          id="minPrice"
           style={{ width: '300px' }}
          className="form-control"
          value={minPrice}
          onChange={event => setMinPrice(event.target.value)}
        />
      </div>
      <div className="form-group">
        <label htmlFor="maxPrice">Max Price:</label>
        <input
          type="number"
          id="maxPrice"
          className="form-control"
          value={maxPrice}
          onChange={event => setMaxPrice(event.target.value)}
        />
      </div>
      <button className="btn btn-primary" onClick={handleSearchByPrice}>
        Search by Price
      </button>
      <h3>Search Results:</h3>
      <ul>
        {searchResults.map(product => (
          <li key={product._id}>{product.name}</li>
        ))}
      </ul>
    </div>
  );
};

export default SearchProduct;
