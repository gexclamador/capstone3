import React from 'react';
import { Button, Row, Col } from 'react-bootstrap';
import { useNavigate } from 'react-router-dom';

export default function Banner() {
  const navigate = useNavigate();

  const redirectToProducts = () => {
    navigate('/products');
  };

  return (
    <Row>
      <Col className="p-5 text-center">
        <h1>CyberspaceReads</h1>
        <p>"Your eBook Wonderland Awaits"!</p>
        <Button variant="primary" onClick={redirectToProducts}>
          Explore Now!
        </Button>
      </Col>
    </Row>
  );
}
