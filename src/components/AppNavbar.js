import React, { useContext } from 'react';
import { Navbar, Nav, Container } from 'react-bootstrap';
import { Link, NavLink } from 'react-router-dom';
import UserContext from '../UserContext';
import  '../App.css';


export default function AppNavbar(){
const { user } = useContext(UserContext);

  return (
    <Navbar  expand="lg" className="custom-navbar">
      <Container fluid>
          <Navbar.Brand as={Link} to='/'>CyberspaceReads</Navbar.Brand>
          
          <Navbar.Toggle aria-controls="basic-navbar-nav"/>
          <Navbar.Collapse id="basic-navbar-nav">
            <Nav className="ms-auto">
              <Nav.Link as={NavLink} to='/'>Home</Nav.Link>
             <Nav.Link as={NavLink} to="/products">Products</Nav.Link>
             
              { (user.id !== null) ?
                  user.isAdmin ?
                    <>
                      
                      <Nav.Link as={NavLink} to='/logout'>Logout</Nav.Link>
                      <Nav.Link as={NavLink} to="/orderdetails" >Orders</Nav.Link>

                    </>
                  :
                    <>
                      {/*<Nav.Link as={NavLink} to='/profile'>Profile</Nav.Link>*/}
                      <Nav.Link as={NavLink} to='/logout'>Logout</Nav.Link>
                      <Nav.Link as={NavLink} to="/orderdetails">Orders</Nav.Link>
                    </>
                : 
                  <>
                    <Nav.Link as={NavLink} to='/register'>Register</Nav.Link>
                    <Nav.Link as={NavLink} to='/login'>Login</Nav.Link>
              
                  </>   
              }
            </Nav>
          </Navbar.Collapse>
      </Container>
    </Navbar>
  )
}