import { useState, useContext } from 'react';
import { Form, Button } from 'react-bootstrap';
import { Navigate, useNavigate } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext.js';

export default function AddCourse() {
  const navigate = useNavigate();
  const { user } = useContext(UserContext);
  const [name, setName] = useState('');
  const [description, setDescription] = useState('');
  const [price, setPrice] = useState(0);
  const [image, setImage] = useState(null); 

  function createProduct(event) {
    event.preventDefault();

    let token = localStorage.getItem('token');
    let formData = new FormData();
    formData.append('name', name);
    formData.append('description', description);
    formData.append('price', price);
    formData.append('image', image); 

    fetch(`${process.env.REACT_APP_API_URL}/api/products`, {
      method: 'POST',
      headers: {
        Authorization: `Bearer ${token}`,
      },
      body: formData,
    })
      .then((response) => response.json())
      .then((result) => {
        if (result) {
          Swal.fire({
            title: 'New product is Added',
            icon: 'success',
          });

          setName('');
          setDescription('');
          setPrice(0);
          setImage(null); // Reset the image state

          navigate('/products');
        } else {
          Swal.fire({
            title: 'Something went wrong',
            text: 'Product creation unsuccessful',
            icon: 'error',
          });
        }
      });
  }

  return (
    (user.isAdmin === true) ? (
      <>
        <h1 className="my-5 text-center">Add Product</h1>
        <Form onSubmit={(event) => createProduct(event)}>
          <Form.Group>
            <Form.Label>Name:</Form.Label>
            <Form.Control
              type="text"
              placeholder="Enter Name"
              required
              value={name}
              onChange={(event) => {
                setName(event.target.value);
              }}
            />
          </Form.Group>
          <Form.Group>
            <Form.Label>Description:</Form.Label>
            <Form.Control
              type="text"
              placeholder="Enter Description"
              required
              value={description}
              onChange={(event) => {
                setDescription(event.target.value);
              }}
            />
          </Form.Group>
          <Form.Group>
            <Form.Label>Price:</Form.Label>
            <Form.Control
              type="number"
              placeholder="Enter Price"
              required
              value={price}
              onChange={(event) => {
                setPrice(event.target.value);
              }}
            />
          </Form.Group>
          <Form.Group>
            <Form.Label>Image:</Form.Label>
            <Form.Control
              type="file"
              accept="image/*"
              onChange={(event) => {
                setImage(event.target.files[0]); // Update the image state
              }}
            />
          </Form.Group>
          <Button variant="primary" type="submit" className="my-5">
            Submit
          </Button>
        </Form>
        <Navigate to="/products" />
      </>
    ) : (
      <Navigate to="/products" />
    )
  );
}
