
import React, { useState, useEffect } from 'react';
import { Table,Button,Modal } from 'react-bootstrap';
import EditProduct from './EditProduct.js';
import ArchiveProduct from './ArchiveProduct.js';
import AddProduct from './AddProduct.js';


export default function AdminView({ productsData, fetchData }) {
const [productsTable, setProductsTable] = useState([]);
const [showAddProductModal, setShowAddProductModal] = useState(false);


  useEffect(() => {
    const productsElements = productsData.map(product => (
      <tr key={product._id}>
        <td>{product._id}</td>
        <td>{product.name}</td>
        <td>{product.description}</td>
        <td>${product.price}</td>
        <td>
          <span className={`text-${product.isActive ? 'success' : 'danger'}`}>
            {product.isActive ? 'Available' : 'Unavailable'}
          </span>
        </td>
        <td>
          <EditProduct product_id={product._id} fetchData={fetchData}/>
        </td>
          <td>
            <ArchiveProduct productId={product._id} fetchData={fetchData} isActive={product.isActive}/>
          </td>
      </tr>
    ));

    setProductsTable(productsElements);
  }, [productsData, fetchData]);

  const openAddProductModal = () => {
  setShowAddProductModal(true);
};

const closeAddProductModal = () => {
  setShowAddProductModal(false);
};

  return (
    <div>

      <h2 className="text-center">Admin Dashboard</h2>

  {/*AddProduct Button*/}
      
   <div className="add-product-button-container">
        <Button
          className="add-product-button"
          variant="primary"
          onClick={openAddProductModal}
        >
          Add Product
        </Button>
      </div>

      <Table striped bordered hover responsive>
        <thead>
          <tr>
            <th>ID</th>
            <th>Name</th>
            <th>Description</th>
            <th>Price</th>
            <th>Availability</th>
            <th>Actions</th>
          </tr>
        </thead>
        <tbody>{productsTable}</tbody>
      </Table>
 <Modal show={showAddProductModal} onHide={closeAddProductModal}>
        <Modal.Header closeButton>
          <Modal.Title>Add Product</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <AddProduct fetchData={fetchData} handleClose={closeAddProductModal} />
        </Modal.Body>
      </Modal>
    </div>
  );
}
